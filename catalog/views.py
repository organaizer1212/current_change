from django.shortcuts import render
from .models import Currency
from django.views import generic

def index(request):
	model = Currency
	num_currency = Currency.objects.all()
	name_currency = Currency.objects.all()
	
	return render(
		request,
		'index.html',
		context = {'num_currency':num_currency[0].value, 'name_currency':num_currency[0].name},
		)

		

# Create your views here.
