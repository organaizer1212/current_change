from django.db import models

class Currency(models.Model):
	"""Model representing a currency ex: Eth,xrp"""

	name = models.CharField(max_length=30, help_text = "Enter a name of currency")
	value = models.FloatField(max_length =10, help_text = "Enter a value of the currency in dollars")

	def __str__(self):
		"""
		String the representing the model object
		"""
		return {'name':self.name,'value':self.value}

# Create your models here.
